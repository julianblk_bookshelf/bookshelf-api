package io.dropwizard.db;

import static io.dropwizard.core.Tables.BOOK;
import static io.dropwizard.core.Tables.BOOKAUTHOR;
import static io.dropwizard.core.Tables.AUTHOR;
import static org.jooq.impl.DSL.using;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;

import io.dropwizard.api.AuthorDTO;
import io.dropwizard.api.BookDTO;


public class BookDAO {

	public static List<BookDTO> findByTitleOrDescriptionPart(String titleOrDescriptionPart) {

		List<BookDTO> books = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
		
		String userName = "";
		String password = "";
		String url = "jdbc:sqlite:bookshelf.db";

		try (Connection conn = DriverManager.getConnection(url, userName, password)) {
			
			System.out.println("titleOrDescriptionPart RECEIVED :: " + titleOrDescriptionPart);
			
			titleOrDescriptionPart = titleOrDescriptionPart.replace("%", "\\%");
			
			System.out.println("titleOrDescriptionPart FIXED :: " + titleOrDescriptionPart);
			
			DSLContext context = using(conn, SQLDialect.SQLITE);
			
			Result<Record> booksResult =
					context
					.select()
					.from(BOOK)
					.where(BOOK.TITLE.like("%"+titleOrDescriptionPart+"%", '\\'))
					.or(BOOK.DESCRIPTION.like("%"+titleOrDescriptionPart+"%", '\\'))
					.fetch();

			for (Record b : booksResult) {
				String isbn = b.getValue(BOOK.ISBN);
				String title = b.getValue(BOOK.TITLE);
				String subtitle = b.getValue(BOOK.SUBTITLE);
				Timestamp published = b.getValue(BOOK.PUBLISHED);
				String publisher = b.getValue(BOOK.PUBLISHER);
				Integer pages = b.getValue(BOOK.PAGES);
				String description = b.getValue(BOOK.DESCRIPTION);
				Boolean instock = b.getValue(BOOK.INSTOCK);

				List<AuthorDTO> authors = new ArrayList<>();
				Result<Record> authorsResult =
						context
						.select()
						.from(BOOKAUTHOR)
						.join(AUTHOR).on(BOOKAUTHOR.AUTHORID.equal(AUTHOR.AUTHORID))
						.where(BOOKAUTHOR.ISBN.eq(isbn))
						.fetch();
				for (Record a : authorsResult) {
					authors.add(new AuthorDTO(a.getValue(AUTHOR.AUTHORID), a.getValue(AUTHOR.NAME)));
				}
				
				books.add(
						BookDTO.builder()
						.isbn(isbn)
						.title(title)
						.subtitle(subtitle)
						.published(published != null ? sdf.format(new Date(published.getTime())) : "")
						.publisher(publisher)
						.pages(pages)
						.description(description)
						.instock(instock)
						.authors(authors)
						.build());
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return books;
	}

	public static List<BookDTO> findAll() {

		List<BookDTO> books = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
		
		String userName = "";
		String password = "";
		String url = "jdbc:sqlite:bookshelf.db";

		try (Connection conn = DriverManager.getConnection(url, userName, password)) {

			Result<Record> result = using(conn, SQLDialect.SQLITE)
					.select()
					.from(BOOK)
					.fetch();

			for (Record r : result) {
				String isbn = r.getValue(BOOK.ISBN);
				String title = r.getValue(BOOK.TITLE);
				String subtitle = r.getValue(BOOK.SUBTITLE);
				Timestamp published = r.getValue(BOOK.PUBLISHED);
				String publisher = r.getValue(BOOK.PUBLISHER);
				Integer pages = r.getValue(BOOK.PAGES);
				String description = r.getValue(BOOK.DESCRIPTION);
				Boolean instock = r.getValue(BOOK.INSTOCK);
				books.add(
						BookDTO.builder()
						.isbn(isbn)
						.title(title)
						.subtitle(subtitle)
						.published(published != null ? sdf.format(new Date(published.getTime())) : "")
						.publisher(publisher)
						.pages(pages)
						.description(description)
						.instock(instock)
						.build());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return books;
	}
}
