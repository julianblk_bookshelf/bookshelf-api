package io.dropwizard;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import io.dropwizard.Application;
import io.dropwizard.resources.BookResource;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class BookshelfApplication extends Application<BookshelfConfiguration> {

    public static void main(final String[] args) throws Exception {
        new BookshelfApplication().run(args);
    }

    @Override
    public String getName() {
        return "Bookshelf";
    }

    @Override
    public void initialize(final Bootstrap<BookshelfConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final BookshelfConfiguration configuration,
                    final Environment environment) {
    	
        final BookResource resource = new BookResource();
        environment.jersey().register(resource);
        

        // Enable CORS headers
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }

}
