package io.dropwizard.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookDTO {
	
	private String isbn;
	private String title;
	private String subtitle;
	private String published;
	private String publisher;
	private Integer pages;
	private String description;
	private Boolean instock;
	private List<AuthorDTO> authors;

    public BookDTO() {
        // Jackson deserialization
    }
	
    @JsonProperty
	public String getIsbn() {
		return isbn;
	}

    @JsonProperty
	public String getTitle() {
		return title;
	}

    @JsonProperty
    public String getSubtitle() {
		return subtitle;
	}

    @JsonProperty
	public String getPublished() {
		return published;
	}

    @JsonProperty
	public String getPublisher() {
		return publisher;
	}

    @JsonProperty
	public Integer getPages() {
		return pages;
	}

    @JsonProperty
	public String getDescription() {
		return description;
	}

    @JsonProperty
	public Boolean getInstock() {
		return instock;
	}

    @JsonProperty
	public List<AuthorDTO> getAuthors() {
		return authors;
	}

	public static Builder builder() {
    	return new Builder();
    }

	public static class Builder {
    	
    	private Builder() {}
    	
		private String isbn;
		private String title;
		private String subtitle;
		private String published;
		private String publisher;
		private Integer pages;
		private String description;
		private Boolean instock;
		List<AuthorDTO> authors;

    	public Builder isbn(String isbn) {
			this.isbn = isbn;
			return this;
		}
    	
		public Builder title(String title) {
			this.title = title;
			return this;
		}

    	public Builder subtitle(String subtitle) {
			this.subtitle = subtitle;
			return this;
		}

    	public Builder published(String published) {
			this.published = published;
			return this;
		}

    	public Builder publisher(String publisher) {
			this.publisher = publisher;
			return this;
		}

    	public Builder pages(Integer pages) {
			this.pages = pages;
			return this;
		}

    	public Builder description(String description) {
			this.description = description;
			return this;
		}

    	public Builder instock(Boolean instock) {
			this.instock = instock;
			return this;
		}

    	public Builder authors(List<AuthorDTO> authors) {
			this.authors = authors;
			return this;
		}
		
		public BookDTO build() {
			BookDTO bookDTO = new BookDTO();
			bookDTO.isbn = this.isbn;
			bookDTO.title = this.title;
			bookDTO.subtitle = this.subtitle;
			bookDTO.published = this.published;
			bookDTO.publisher = this.publisher;
			bookDTO.pages = this.pages;
			bookDTO.description = this.description;
			bookDTO.instock = this.instock;
			bookDTO.authors = this.authors;
			return bookDTO;
		}
    }
}
