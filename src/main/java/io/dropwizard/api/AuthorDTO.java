package io.dropwizard.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorDTO {
	
	private Integer id;
	private String name;

    public AuthorDTO() {
        // Jackson deserialization
    }
    
    public AuthorDTO(Integer id, String name) {
    	this.id = id;
    	this.name = name;
    }

    @JsonProperty
	public Integer getId() {
		return id;
	}
    
    @JsonProperty
	public String getName() {
		return name;
	}
}
