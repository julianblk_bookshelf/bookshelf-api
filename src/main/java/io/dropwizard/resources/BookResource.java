package io.dropwizard.resources;


import java.util.List;
import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.dropwizard.api.BookDTO;
import io.dropwizard.db.BookDAO;

@Path("/book")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

    @GET
    @Path("/findByTitleOrDescriptionPart")
    public List<BookDTO> findByTitleOrDescriptionPart(@QueryParam("titleOrDescriptionPart") Optional<String> titleOrDescriptionPart) {
        return BookDAO.findByTitleOrDescriptionPart(titleOrDescriptionPart.get());
    }

    @GET
    @Path("/dummy")
    public BookDTO sayHello() {
        return BookDTO.builder().isbn("DUMMY-978-1617291999").title("DUMMY - Java 8 in Action").build();
    }
}
