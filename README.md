# Bookshelf

How to start the Bookshelf application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/bookshelf-api-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

SQLite-JOOQ Code Generation
---

1. cd to jooq_generator
1. Run `java -classpath "jooq-3.10.7.jar;jooq-meta-3.10.7.jar;jooq-codegen-3.10.7.jar;sqlite-jdbc-3.23.1.jar;." org.jooq.util.GenerationTool library.xml`
1. JOOQ generated sources are in the `jooq_generator/jooq_generated` folder
1. Update the source files in the `io.dropwizard.core` package